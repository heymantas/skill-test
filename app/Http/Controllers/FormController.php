<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Message;
use Illuminate\Http\Request;

class FormController extends Controller
{
    public function index()
    {
        $countries = Country::all();
        return view('form', array('countries' => $countries));
    }

    public function store(Request $request)
    {

        $request->validate([
            'name' => "required",
            'email' => "required|email",
            'country_id' => "required|numeric"
        ]);

        $message = new Message();
        $message->name = $request->name;
        $message->email = $request->email;
        $message->country_id = $request->country_id;
        $message->save();

        $createdMessage = Message::where('id', $message->id)
            ->with('country')
            ->first();

        return response()->json($createdMessage);
    }
}
