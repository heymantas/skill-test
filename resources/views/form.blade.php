<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Countribee's skill test</title>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;600;700&display=swap" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link href="{{asset('/css/app.css')}}" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="bigText">
        Contribee’s skill test
    </div>
    <div class="smallText">
        Simple form using AJAX and Laravel
    </div>
    <input name="name" placeholder="Full name" class="nameInput"/>
    <input name="email" type="email" placeholder="E-mail address" class="emailInput"/>
    <select name="country_id" class="countryInput" required>
        <option selected disabled hidden>
            Select a country
        </option>
        @foreach ($countries as $country)
            <option value="{{$country->id}}">{{$country->name}}</option>
        @endforeach
    </select>
    <button class="sendButton">Send</button>
</div>
</body>
<script src="{{asset('/js/app.js')}}"></script>
</html>
