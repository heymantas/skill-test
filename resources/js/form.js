$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(".sendButton").click(function () {
    const name = $("input[name=name]").val();
    const email = $("input[name=email]").val();
    const country_id = $("select[name=country_id]").val();

    $.ajax({
        type: 'POST',
        url: "/save",
        data: {
            name: name,
            email: email,
            country_id: country_id
        },
        success: function (data) {
            swal({
                title: `Your message (#${data.id}) has been sent.`,
                text: `Greetings from ${data.country.name}`,
                icon: "success",
            });
        },

        error: function (data) {
            swal({
                title: `${data.responseJSON.message}`,
                icon: "error",
            });
        }
    });
});
